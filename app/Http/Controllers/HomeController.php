<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $brands = Brand::all();
        $min_price = Product::min('price');
        $max_price = Product::max('price');
        if (!empty(request()->all())) {
            $where = '';
            $filter = $request->all();
            if (array_key_exists('search', $filter)) {
                if ($filter['search'] != null) {
                    $where .= " name like '%".$filter['search']."%' and";
                }
            }
            if (array_key_exists('brand', $filter)) {
                if ($filter['brand'] != null) {
                    $where .= " brand_id = ".$filter['brand']." and";
                }

            }
            if (array_key_exists('price_from', $filter)) {
                if ($filter['price_from'] != null) {
                    $where .= " price >= ".$filter['price_from']." and";
                }

            }
            if (array_key_exists('price_to', $filter)) {
                if ($filter['price_to'] != null) {
                    $where .= " price <= ".$filter['price_to']." and";
                }
            }
            $products = Product::whereRaw(trim($where,'and'))->get();
        }else{
            $products = Product::all();
        }
        return view('index', compact('products', 'brands', 'min_price', 'max_price'));
    }
}
