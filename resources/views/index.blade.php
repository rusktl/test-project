@extends('layouts.app')
@section('content')
    <div class="wrapper">
        <form action="/" method="get">
            <div class="row">
                <div class="col-lg-2 col-sm-3">
                    <select id="cokeId" name="brand">
                        <option value=null disabled
                                @if(!isset($_GET['brand']) || (isset($_GET['brand']) && $_GET['brand'] == null)) selected @endif>
                            Choose brand
                        </option>
                        @foreach($brands as $brand)
                            <option value="{{$brand->id}}"
                                    @if(isset($_GET) && isset($_GET['brand']) && $_GET['brand'] == $brand->id) selected @endif>{{$brand->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label for="price_from">Price From</label>
                    <input type="number" name="price_from"
                           @if(isset($_GET) && isset($_GET['price_from']) && $_GET['price_from'] != '') value="{{$_GET['price_from']}}"
                           @else value="{{$min_price ?? ''}}" @endif id="price_from">
                    <label for="price_to">Price To</label>
                    <input type="number" name="price_to" id="price_to"
                           @if(isset($_GET) && isset($_GET['price_to']) && $_GET['price_to'] != null) value="{{$_GET['price_to']}}"
                           @else value="{{$max_price ?? ''}}" @endif>
                </div>
                <div class="col-lg-2">
                    <input type="text" name="search"
                           @if(isset($_GET) && isset($_GET['search']) && $_GET['search'] != null) value="{{$_GET['search']}}"
                           @endif placeholder="Search">
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <button id="findBtn" class="btn btn-success" type="submit">Find</button>
                        <a href="/" id="findBtn" class="btn btn-danger">Reset</a>
                    </div>

                </div>
            </div>
        </form>
    </div>
    <div id="productData">
        <table class="table table-bordered">
            <tr>
                <th with="80px">No</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Brand</th>
            </tr>
            @foreach($products as $key => $product)
                <tr>
                    <td with="80px">{{$key + 1}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->brand->name}}</td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection
