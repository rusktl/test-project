@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }} Theme
        </div>

        <div class="card-body">
            <form action="{{ route("admin.brands.update",[$brand->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Name*</label>
                    <input type="text" id="title" name="name" value="{{$brand->name}}" required class="form-control" >
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
@endsection
