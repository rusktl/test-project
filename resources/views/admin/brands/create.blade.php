@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} Theme
    </div>

    <div class="card-body">
        <form action="{{ route("admin.brands.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Name*</label>
                <input type="text" id="name" name="name" required class="form-control" >
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
    @endsection
